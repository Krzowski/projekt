﻿using System.Collections.Specialized;
using CommonClasses;
using Lidgren.Network;

namespace WinServer
{
    internal class ConnectedClient
    {
        public NetConnection NetConnection;
        public int Id { get; set; }

        public int PassedTurns { get; set; } = 0;
        public ScoreDto Score { get; set; }

        public ConnectedClient(NetConnection nC, int i)
        {
            Score = new ScoreDto() {Name = "", Score = 0};
            NetConnection = nC;
            Id = i;
        }
    }
}