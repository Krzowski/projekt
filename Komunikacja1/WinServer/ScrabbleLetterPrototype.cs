﻿using System.Collections.Generic;

namespace WinServer
{
    internal class ScrabbleLetterPrototype
    {
        public char Letter { get; set; }
        public int Count { get; set; }
        public int Score { get; set; }

        public static List<ScrabbleLetterPrototype> GeneratePrototypes()
        {
            return new List<ScrabbleLetterPrototype>
            {
                new ScrabbleLetterPrototype {Letter = 'E', Count = 12, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'A', Count = 9, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'I', Count = 9, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'O', Count = 8, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'N', Count = 6, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'R', Count = 6, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'T', Count = 6, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'L', Count = 4, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'S', Count = 4, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'U', Count = 4, Score = 1},
                new ScrabbleLetterPrototype {Letter = 'D', Count = 4, Score = 2},
                new ScrabbleLetterPrototype {Letter = 'G', Count = 3, Score = 2},
                new ScrabbleLetterPrototype {Letter = 'B', Count = 2, Score = 3},
                new ScrabbleLetterPrototype {Letter = 'C', Count = 2, Score = 3},
                new ScrabbleLetterPrototype {Letter = 'M', Count = 2, Score = 3},
                new ScrabbleLetterPrototype {Letter = 'P', Count = 2, Score = 3},
                new ScrabbleLetterPrototype {Letter = 'F', Count = 2, Score = 4},
                new ScrabbleLetterPrototype {Letter = 'H', Count = 2, Score = 4},
                new ScrabbleLetterPrototype {Letter = 'V', Count = 2, Score = 4},
                new ScrabbleLetterPrototype {Letter = 'W', Count = 2, Score = 4},
                new ScrabbleLetterPrototype {Letter = 'Y', Count = 2, Score = 4},
                new ScrabbleLetterPrototype {Letter = 'K', Count = 1, Score = 5},
                new ScrabbleLetterPrototype {Letter = 'J', Count = 1, Score = 8},
                new ScrabbleLetterPrototype {Letter = 'X', Count = 1, Score = 8},
                new ScrabbleLetterPrototype {Letter = 'Q', Count = 1, Score = 10},
                new ScrabbleLetterPrototype {Letter = 'Z', Count = 1, Score = 10}
            };
        }
    }
    
}