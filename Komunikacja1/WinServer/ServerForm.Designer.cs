﻿using System.ComponentModel;
using System.Windows.Forms;

namespace WinServer
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartButton = new System.Windows.Forms.Button();
            this.NextClientButton = new System.Windows.Forms.Button();
            this.ClientCountLabel = new System.Windows.Forms.Label();
            this.CurrentClientLabel = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(7, 33);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(117, 23);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "Start Game";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // NextClientButton
            // 
            this.NextClientButton.Enabled = false;
            this.NextClientButton.Location = new System.Drawing.Point(133, 33);
            this.NextClientButton.Name = "NextClientButton";
            this.NextClientButton.Size = new System.Drawing.Size(117, 23);
            this.NextClientButton.TabIndex = 1;
            this.NextClientButton.Text = "Next Client";
            this.NextClientButton.UseVisualStyleBackColor = true;
            this.NextClientButton.Click += new System.EventHandler(this.NextClientButton_Click);
            // 
            // ClientCountLabel
            // 
            this.ClientCountLabel.AutoSize = true;
            this.ClientCountLabel.Location = new System.Drawing.Point(130, 7);
            this.ClientCountLabel.Name = "ClientCountLabel";
            this.ClientCountLabel.Size = new System.Drawing.Size(69, 13);
            this.ClientCountLabel.TabIndex = 2;
            this.ClientCountLabel.Text = "Client count: ";
            // 
            // CurrentClientLabel
            // 
            this.CurrentClientLabel.AutoSize = true;
            this.CurrentClientLabel.Location = new System.Drawing.Point(130, 20);
            this.CurrentClientLabel.Name = "CurrentClientLabel";
            this.CurrentClientLabel.Size = new System.Drawing.Size(72, 13);
            this.CurrentClientLabel.TabIndex = 3;
            this.CurrentClientLabel.Text = "Current client:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(79, 7);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Turn length:";
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 64);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CurrentClientLabel);
            this.Controls.Add(this.ClientCountLabel);
            this.Controls.Add(this.NextClientButton);
            this.Name = "ServerForm";
            this.Text = "Scrabble Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ServerForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private Button StartButton;
        private Button NextClientButton;
        private Label ClientCountLabel;
        private Label CurrentClientLabel;
        private NumericUpDown numericUpDown1;
        private Label label1;
    }
}

