﻿using System;
using System.Threading;
using System.Windows.Forms;
using CommonClasses;

namespace WinServer
{
    public partial class ServerForm : Form
    {
        private readonly Thread _acceptClientsThread;
        private readonly Thread _labelsThread;

        public ServerForm()
        {
            InitializeComponent();
            Fred.ByeThreads = false;
            ScrabbleServer.Setup();
            _acceptClientsThread = new Thread(ScrabbleServer.AcceptClients);
            Fred.AddStart(_acceptClientsThread);
            _labelsThread = new Thread(() =>
            {
                while (!Fred.ByeThreads)
                {
                    Thread.Sleep(1);
                    ClientCountLabel.Text = "Client count: " + ScrabbleServer.GetClientCount();
                    CurrentClientLabel.Text = "Current client: " + (ScrabbleServer.GetCurrentClient() + 1);
                }
            }
                );
            Fred.AddStart(_labelsThread);
        }
        private void StartButton_Click(object sender, EventArgs e)
        {
            if (!ScrabbleServer.CanStartGame) return;
            if (ScrabbleServer.IsGameStarted) return;
            ScrabbleServer.StartGame();
            var clicoThread = new Thread(ScrabbleServer.ClientControl);
            Fred.AddStart(clicoThread);
            numericUpDown1.Enabled = false;
            StartButton.Enabled = false;
            NextClientButton.Enabled = true;
            Thread.Sleep(1);
        }

        private void NextClientButton_Click(object sender, EventArgs e)
        {
            if (!ScrabbleServer.IsGameStarted) return;
            ScrabbleServer.StopAllClients();
            ScrabbleServer.NextTurn();
        }

        private void ServerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ScrabbleServer.CancelGame();
            Fred.JoinAll();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            ScrabbleServer.TurnLength = (int)numericUpDown1.Value;
        }

       
    }
}
