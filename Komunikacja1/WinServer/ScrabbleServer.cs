﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Timers;
using System.Xml.Serialization;
using CommonClasses;
using Lidgren.Network;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace WinServer
{
    static class ScrabbleServer
    {
        private static Random _rng = new Random();
        private static bool _shouldReceiveBoard;
        public static int TurnLength=90;
        private static int _doublePassCounter;
        private static bool _gameOver;
        public static void Shuffle()
        {
            int n = LetterBank.Count;
            while (n > 1)
            {
                n--;
                int k = _rng.Next(n + 1);
                var value = LetterBank[k];
                LetterBank[k] = LetterBank[n];
                LetterBank[n] = value;
            }
        }
        public static bool CanStartGame { get; set; }
        static List<ConnectedClient> _clientList;
        static NetServer _server;
        public static int ClientTurnCounter { get; private set; }
        public static bool IsGameStarted { get; set; }
        public static List<ScrabbleLetter> LetterBank { get; set; } = new List<ScrabbleLetter>();
        public static void SendInitialLettersToClients()
        {
            foreach (var c in _clientList)
            {
                SendNextNLettersToClient(7,c);
            }
        }

        public static void GenerateLetters()
        {
            var proto = ScrabbleLetterPrototype.GeneratePrototypes();
            foreach (var p in proto)
            {
                for (int i = 0; i < p.Count; i++)
                {
                    LetterBank.Add(new ScrabbleLetter {Char = p.Letter, Score=p.Score});
                }
            }
            Shuffle();
        }

        public static void SendNextNLettersToClient(int n, ConnectedClient client)
        {
            if (n == 0)
            {
                client.PassedTurns++;
                if (client.PassedTurns >= 2) _doublePassCounter++;
            }
            else
            {
                client.PassedTurns = 0;
                _doublePassCounter = 0;
            }
            if (n == 7 && LetterBank.Count == 0)
            {
                _gameOver = true;
            }
            var toSend = new List<ScrabbleLetter>();
            int m = Math.Min(LetterBank.Count, n);
            toSend.AddRange(LetterBank.GetRange(0, m));
            LetterBank.RemoveRange(0,m);
            XmlSerializer x = new XmlSerializer(typeof(List<ScrabbleLetter>));
            var sw = new StringWriter();
            x.Serialize(sw, toSend);
            string res = sw.ToString();
            MessageClient(client.NetConnection, res);
        }

        public static void ClientControl()
        {
            /*var countTime = new Timer {Interval = TurnLength*1000};
            countTime.Elapsed += OnTimedEvent;
            countTime.AutoReset = true;
            countTime.Enabled = true;
            countTime.Start();*/
            NetIncomingMessage inc;
            while (!_gameOver && !Fred.ByeThreads)
            {
                DateTime TurnEnd = DateTime.Now + new TimeSpan(0,0,TurnLength);
                while (TurnEnd.Subtract(DateTime.Now) > TimeSpan.Zero && !Fred.ByeThreads)
                {
                    if ((inc = _server.ReadMessage()) == null) continue;
                    if (inc.MessageType == NetIncomingMessageType.Data)
                    {
                        var result = inc.ReadString();
                        if(result==Commands.EndTurn) break;
                    }
                    Thread.Sleep(1);
                }
                if (Fred.ByeThreads) return;
                StopAllClients();
                NextTurn();
            }
        }

        private static void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            if (_gameOver) return;
            if (_clientList.Count == 0) return;
        }

        public static void NextTurn()
        {
            string boardString;
            if (_shouldReceiveBoard)
            {
                int nLettersToSend;
                boardString = ReceiveBoard();
                nLettersToSend = int.Parse(boardString[0].ToString());
                boardString = boardString.Remove(0, 1);
                int m = GetPreviousClient();
                SendNextNLettersToClient(nLettersToSend,_clientList[m]);
                GameStateDto g = JsonConvert.DeserializeObject<GameStateDto>(boardString);
                _clientList[m].Score = g.myScore;
                g.Scores = GetScoreboard();
                boardString = JsonConvert.SerializeObject(g);
            }
            else boardString = "";
            foreach (var c in _clientList)
            {
                MessageClient(c.NetConnection, boardString);
            }
            int n = GetCurrentClient();
            MessageClient(_clientList[n].NetConnection, Commands.Turn);
            ClientTurnCounter++;
            _shouldReceiveBoard = true;
            if (CheckDoublePassGameOver())
            {
                _gameOver = true;
            }
            if (_gameOver)
            {
                foreach (var client in _clientList)
                {
                    MessageClient(client.NetConnection, Commands.GameOver);
                }
            }
        }

        private static List<ScoreDto> GetScoreboard()
        {
            List<ScoreDto> list = new List<ScoreDto>();
            foreach (var c in _clientList)
            {
                list.Add(new ScoreDto() { Name = c.Score.Name, Score = c.Score.Score});
            }
            return list;
        }

        private static bool CheckDoublePassGameOver()
        {
            return _doublePassCounter >= _clientList.Count;
        }

        private static int GetPreviousClient()
        {
            if (_clientList.Count == 0) return -1;
            return (ClientTurnCounter - 1) % _clientList.Count;
        }

        private static string ReceiveBoard()
        {
            NetIncomingMessage inc;
            string result = "";
            while (!Fred.ByeThreads)
            {
                Thread.Sleep(1);
                if ((inc = _server.ReadMessage()) == null) continue;
                if (inc.MessageType == NetIncomingMessageType.Data)
                {
                    result = inc.ReadString();
                    break;
                }
            }
            return result;
        }

        private static void MessageClient(NetConnection n, string s)
        {
            var msg = _server.CreateMessage();
            msg.Write(s);
            _server.SendMessage(msg, n, NetDeliveryMethod.ReliableOrdered);
        }

        public static void StopAllClients()
        {
            foreach (var con in _clientList)
            {
                MessageClient(con.NetConnection,Commands.Wait);
            }
        }

        public static void Setup()
        {
            GenerateLetters();
            _clientList = new List<ConnectedClient>();
            var config = new NetPeerConfiguration("kom1") {Port = 14242};
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            _server = new NetServer(config);
            _server.Start();
        }

        public static int GetClientCount()
        {
            return _clientList.Count;
        }

        public static void AcceptClients()
        {
            NetIncomingMessage inc;
            while (!IsGameStarted && !Fred.ByeThreads)
            {
                Thread.Sleep(1);
                if ((inc = _server.ReadMessage()) == null) continue;
                if (inc.MessageType == NetIncomingMessageType.ConnectionApproval)
                {
                    var enteredPassword = inc.ReadString();
                    if (CheckPassword(enteredPassword))
                        AcceptClient(inc);
                    else
                        DenyClient(inc);
                }
            }
        }

        private static void DenyClient(NetIncomingMessage inc)
        {
            var response = _server.CreateMessage();
            inc.SenderConnection.Deny();
            response.Write("-1");
            _server.SendUnconnectedMessage(response, inc.SenderEndPoint);
        }

        private static void AcceptClient(NetIncomingMessage inc)
        {
            var response = _server.CreateMessage();
            inc.SenderConnection.Approve();
            _clientList.Add(new ConnectedClient(inc.SenderConnection, _clientList.Count + 1));
            response.Write(_clientList.Count.ToString());
            _server.SendUnconnectedMessage(response, inc.SenderEndPoint);
            if (_clientList.Count > 1) CanStartGame = true;
        }

        private static bool CheckPassword(string enteredPassword)
        {
            return enteredPassword.Equals("aligator1");
        }

        public static void StartGame()
        {
            foreach (var con in _clientList)
            {
                MessageClient(con.NetConnection, Commands.GameStarted);
            }
            SendInitialLettersToClients();
            IsGameStarted = true;
            NextTurn();
        }

        public static void CancelGame()
        {
            foreach (var con in _clientList)
            {
                MessageClient(con.NetConnection, Commands.GameCanceled);
            }
        }

        public static int GetCurrentClient()
        {
            if (_clientList.Count == 0) return -1;
            return ClientTurnCounter % _clientList.Count;
        }
    }
}