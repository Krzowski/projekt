﻿namespace CommonClasses
{
    public static class Commands
    {
        public static readonly string Turn = "!01";
        public static readonly string Wait = "!02";
        public static readonly string GameStarted = "!03";
        public static readonly string GameCanceled = "!04";
        public static readonly string GameOver = "!05";
        public static readonly string EndTurn = "!06";
    }
}
