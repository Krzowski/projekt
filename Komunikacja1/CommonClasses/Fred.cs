﻿using System.Collections.Generic;
using System.Threading;

namespace CommonClasses
{
    public static class Fred // Fred the Thread Manager
    {
        public static bool ByeThreads { get; set; }
        public static List<Thread> ThreadList = new List<Thread>();

        public static void JoinAll()
        {
            ByeThreads = true;
            foreach (var t in ThreadList)
            {
                t.Join();
            }
        }

        public static void AddStart(Thread t)
        {
            ThreadList.Add(t);
            t.Start();
        }
    }
}
