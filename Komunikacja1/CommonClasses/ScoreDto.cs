﻿namespace CommonClasses
{
    public class ScoreDto
    {
        private ScoreDto x;

        public ScoreDto()
        {
            
        }
        public ScoreDto(ScoreDto x)
        {
            Name = x.Name;
            Score = x.Score;
        }

        public string Name { get; set; }
        public int Score { get; set; }

        public override string ToString()
        {
            return Name + " - " + Score + " points";
        }
    }
}