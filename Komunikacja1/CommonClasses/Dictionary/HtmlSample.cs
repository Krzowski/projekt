﻿using System.Net;
using System.Text;

namespace CommonClasses
{
    public class HtmlSample
    {
        private readonly string _url;

        public HtmlSample(string url)
        {
            _url = url;
        }
        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;

                var html = WebUtility.HtmlDecode(wc.DownloadString(_url));
                return html;
            }
        }
    }
}
