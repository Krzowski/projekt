﻿using System.Collections.Generic;

namespace CommonClasses
{
    public class Head
    {
    }
    public class Syn
    {
        public string Text { get; set; }
        public string Pos { get; set; }
        public string Gen { get; set; }
    }
    public class Mean
    {
        public string Text { get; set; }
    }
    public class Tr2
    {
        public string Text { get; set; }
    }
    public class Ex
    {
        public string Text { get; set; }
        public List<Tr2> Tr { get; set; }
    }
    public class Tr
    {
        public string Text { get; set; }
        public string Pos { get; set; }
        public string Gen { get; set; }
        public List<Syn> Syn { get; set; }
        public List<Mean> Mean { get; set; }
        public List<Ex> Ex { get; set; }
        public string Asp { get; set; }
    }
    public class Def
    {
        public string Text { get; set; }
        public string Pos { get; set; }
        public string Ts { get; set; }
        public List<Tr> Tr { get; set; }
    }
    public class RootObject
    {
        public Head Head { get; set; }
        public List<Def> Def { get; set; }
        public List<Syn> Syn { get; set; }
    }
}
