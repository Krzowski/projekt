﻿using Newtonsoft.Json;

namespace CommonClasses
{
    public static class Dictionary
    {
        private static string _url =
            "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20160416T160210Z.2cee43ecdd84fc74.7024416c159fae352a1c3bdd0b4127e2c8958bb9&lang=en-ru&text=";
        public static bool CheckWord(string word)
        {
            var site = new HtmlSample(_url + word);
            var api = site.GetPageHtml();
            var output = JsonConvert.DeserializeObject(api);
            var deserializedWord = JsonConvert.DeserializeObject<RootObject>(output.ToString());
            return deserializedWord.Def.Count != 0;
        }
    }
}
