﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClasses
{
    public class GameStateDto
    {
        public List<ScoreDto> Scores;
        public List<ScrabbleLetter> board;
        public ScoreDto myScore;
    }
}
