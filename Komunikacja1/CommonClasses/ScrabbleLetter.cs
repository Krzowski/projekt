﻿using System;

namespace CommonClasses
{
    [Serializable]
    public class ScrabbleLetter
    {
        public char Char { get; set; }
        public int Score { get; set; }
        public int TileScore { get; set; }
        public ScrabbleLetter()
        {
            
        }

        public ScrabbleLetter(char c)
        {
            Char = c;
        }

        public override string ToString()
        {
            return Char.ToString();
        }
    }
}
