﻿using System.Drawing;


namespace WinClient
{
    public static class AppStyle
    {
        public static Font TileFont = new Font("Microsoft Sans Serif", 8F, FontStyle.Bold, GraphicsUnit.Point, 238);
        public static int TileSize = 25;
        public static string BoardString = "100200010002001" +
                                           "030004000400030" +
                                           "003000202000300" +
                                           "200300020003002" +
                                           "000030000030000" +
                                           "040004000400040" +
                                           "002000202000200" +
                                           "100200050002001" +
                                           "002000202000200" +
                                           "040004000400040" +
                                           "000030000030000" +
                                           "200300020003002" +
                                           "003000202000300" +
                                           "030004000400030" +
                                           "100200010002001";

        public static int[,] TabBonusWordX3 = { { 0, 0 }, { 7, 0 }, { 14, 0 }, { 0, 7 }, { 14, 7 }, { 0, 14 }, { 7, 14 }, { 14, 14 } };

        public static int[,] TabBonusWordX2 =
        {
            {1,1}, {13,1}, {2,2}, {12,2}, {3,3}, {11,3}, {4,4}, {10,4}, {4,10}, {10,10}, {3,11},{11,11},{2,12},{12,12},{1,13},
            {13,13}
        };

        public static int[,] TabBonusLetterX2 =
        {
            {3,0}, {11,0}, {6,2}, {8,2}, {0,3}, {7,3}, {14,3},{2,6},{6,6},{8,6},{12,6},{3,7},{11,7},{2,8},{6,8},{8,8},{12,8},
            {0,11},{7,11},{14,11},{6,12},{8,12},{3,14},{11,14}

        };

        public static int[,] TabBonusLetterX3 =
        {
            {5, 1}, {9, 1}, {1, 5}, {5, 5}, {9, 5}, {13, 5}, {1, 9}, {5, 9}, {9, 9}, {13, 9}, {5, 13}, {9, 13}
        };
        public static class Colors
        {
            public static readonly Color BonusWordX3 = Color.DarkRed;
            public static readonly Color Center = Color.Brown;
            public static readonly Color BonusWordX2 = Color.OrangeRed;
            public static readonly Color BonusLetterX2 = Color.DodgerBlue;
            public static readonly Color BonusLetterX3 = Color.DarkBlue;
            public static readonly Color Empty = Color.DarkGreen;
            public static readonly Color Inactive = Color.DarkGoldenrod;
            public static readonly Color Active = Color.AntiqueWhite;
            
        }
    }
}
