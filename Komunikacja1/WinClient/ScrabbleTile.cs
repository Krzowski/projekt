using System.Drawing;
using System.Windows.Forms;
using CommonClasses;

namespace WinClient
{
    public class ScrabbleTile : Button
    {
        public ScrabbleLetter Letter { get; set; }
        public bool Active { get; set; } = true;
        public Multiplier Mul { get; set; }

        public ScrabbleTile()
        {
            FlatStyle = FlatStyle.Flat;
            Width = AppStyle.TileSize;
            Height = AppStyle.TileSize;
            Font = AppStyle.TileFont;
            TextAlign = ContentAlignment.MiddleCenter;
        }
        public bool Empty()
        {
            return Letter == null;
        }
        
        public virtual void UpdateStyle()
        {
            if (Letter == null)
            {
                Text = "";
            }
            else
            {
                BackColor = Active ? AppStyle.Colors.Active : AppStyle.Colors.Inactive;
                Text = Letter.ToString();
            }
        }

        public void EmptyTile( int xpos, int ypos)
        {
            for (int i = 0; i <= 14; i++)
            {
                for (int j = 0; j <= 14; j++)
                {
                    if (xpos == i && ypos == j) BackColor = AppStyle.Colors.Empty;
                }
            }
        }

    }

    public class ScrabbleTileInHand : ScrabbleTile
    {
        
    }
    public class ScrabbleTileOnBoard : ScrabbleTile
    {
        public int Xpos { get; set; }
        public int Ypos { get; set; }
        public override void UpdateStyle()
        {
            if (Letter == null)
            {
                EmptyTile(Xpos, Ypos);
                BackColor = Xpos == 7 && Ypos == 7 ? AppStyle.Colors.Center : AppStyle.Colors.Empty;

                for (int i = 0; i < AppStyle.TabBonusWordX3.Length/2; i++)
                {
                    if ((Xpos == AppStyle.TabBonusWordX3[i, 0] && Ypos == AppStyle.TabBonusWordX3[i, 1])) BackColor = AppStyle.Colors.BonusWordX3;
                }

                for (int i = 0; i < AppStyle.TabBonusWordX2.Length / 2; i++)
                {
                    if ((Xpos == AppStyle.TabBonusWordX2[i, 0] && Ypos == AppStyle.TabBonusWordX2[i, 1])) BackColor = AppStyle.Colors.BonusWordX2;
                }
                
                for (int i = 0; i < AppStyle.TabBonusLetterX2.Length / 2; i++)
                {
                    if ((Xpos == AppStyle.TabBonusLetterX2[i, 0] && Ypos == AppStyle.TabBonusLetterX2[i, 1])) BackColor = AppStyle.Colors.BonusLetterX2;
                }
                for (int i = 0; i < AppStyle.TabBonusLetterX3.Length / 2; i++)
                {
                    if ((Xpos == AppStyle.TabBonusLetterX3[i, 0] && Ypos == AppStyle.TabBonusLetterX3[i, 1])) BackColor = AppStyle.Colors.BonusLetterX3;
                }
                Text = "";
            }
            else
            {
                BackColor = Active ? AppStyle.Colors.Active : AppStyle.Colors.Inactive;
                Text = Letter.ToString();
            }
        }
    }
}