﻿using System.ComponentModel;
using System.Windows.Forms;

namespace WinClient
{
    partial class ScrabblePlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boardPanel = new System.Windows.Forms.Panel();
            this.handPanel = new System.Windows.Forms.Panel();
            this.pickedLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nickBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TurnLabel = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.portBox = new System.Windows.Forms.TextBox();
            this.IpBox = new System.Windows.Forms.TextBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.IdLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.endTurnButton = new System.Windows.Forms.Button();
            this.scoreBox = new System.Windows.Forms.ListBox();
            this.backToHandButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // boardPanel
            // 
            this.boardPanel.AutoSize = true;
            this.boardPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.boardPanel.Location = new System.Drawing.Point(3, 3);
            this.boardPanel.MinimumSize = new System.Drawing.Size(100, 100);
            this.boardPanel.Name = "boardPanel";
            this.boardPanel.Size = new System.Drawing.Size(100, 100);
            this.boardPanel.TabIndex = 3;
            // 
            // handPanel
            // 
            this.handPanel.AutoSize = true;
            this.handPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.handPanel.Location = new System.Drawing.Point(3, 199);
            this.handPanel.MinimumSize = new System.Drawing.Size(100, 100);
            this.handPanel.Name = "handPanel";
            this.handPanel.Size = new System.Drawing.Size(100, 100);
            this.handPanel.TabIndex = 4;
            // 
            // pickedLabel
            // 
            this.pickedLabel.BackColor = System.Drawing.Color.Wheat;
            this.pickedLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pickedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pickedLabel.Location = new System.Drawing.Point(3, 0);
            this.pickedLabel.Name = "pickedLabel";
            this.pickedLabel.Size = new System.Drawing.Size(25, 25);
            this.pickedLabel.TabIndex = 3;
            this.pickedLabel.Text = "X";
            this.pickedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.pickedLabel.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.boardPanel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.handPanel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(312, 302);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.nickBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.connectButton);
            this.panel1.Controls.Add(this.portBox);
            this.panel1.Controls.Add(this.IpBox);
            this.panel1.Controls.Add(this.passwordBox);
            this.panel1.Controls.Add(this.StatusLabel);
            this.panel1.Controls.Add(this.IdLabel);
            this.panel1.Location = new System.Drawing.Point(109, 199);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 100);
            this.panel1.TabIndex = 5;
            // 
            // nickBox
            // 
            this.nickBox.Location = new System.Drawing.Point(50, 70);
            this.nickBox.Name = "nickBox";
            this.nickBox.Size = new System.Drawing.Size(134, 20);
            this.nickBox.TabIndex = 15;
            this.nickBox.Text = "no name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "My nick: ";
            // 
            // TurnLabel
            // 
            this.TurnLabel.AutoSize = true;
            this.TurnLabel.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TurnLabel.Location = new System.Drawing.Point(3, 25);
            this.TurnLabel.Name = "TurnLabel";
            this.TurnLabel.Size = new System.Drawing.Size(131, 31);
            this.TurnLabel.TabIndex = 13;
            this.TurnLabel.Text = "My turn?";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(109, 41);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 12;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // portBox
            // 
            this.portBox.Location = new System.Drawing.Point(3, 23);
            this.portBox.Name = "portBox";
            this.portBox.Size = new System.Drawing.Size(100, 20);
            this.portBox.TabIndex = 11;
            this.portBox.Text = "14242";
            // 
            // IpBox
            // 
            this.IpBox.Location = new System.Drawing.Point(3, 3);
            this.IpBox.Name = "IpBox";
            this.IpBox.Size = new System.Drawing.Size(100, 20);
            this.IpBox.TabIndex = 10;
            this.IpBox.Text = "127.0.0.1";
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(3, 44);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(100, 20);
            this.passwordBox.TabIndex = 9;
            this.passwordBox.Text = "aligator1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(109, 16);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(58, 13);
            this.StatusLabel.TabIndex = 8;
            this.StatusLabel.Text = "My status: ";
            // 
            // IdLabel
            // 
            this.IdLabel.AutoSize = true;
            this.IdLabel.Location = new System.Drawing.Point(109, 3);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Size = new System.Drawing.Size(38, 13);
            this.IdLabel.TabIndex = 7;
            this.IdLabel.Text = "My id: ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.backToHandButton);
            this.panel2.Controls.Add(this.endTurnButton);
            this.panel2.Controls.Add(this.scoreBox);
            this.panel2.Controls.Add(this.TurnLabel);
            this.panel2.Controls.Add(this.pickedLabel);
            this.panel2.Location = new System.Drawing.Point(109, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 190);
            this.panel2.TabIndex = 6;
            // 
            // endTurnButton
            // 
            this.endTurnButton.BackColor = System.Drawing.Color.DarkOrange;
            this.endTurnButton.Location = new System.Drawing.Point(9, 161);
            this.endTurnButton.Name = "endTurnButton";
            this.endTurnButton.Size = new System.Drawing.Size(175, 23);
            this.endTurnButton.TabIndex = 12;
            this.endTurnButton.Text = "END TURN";
            this.endTurnButton.UseVisualStyleBackColor = false;
            this.endTurnButton.Click += new System.EventHandler(this.endTurnButton_Click);
            // 
            // scoreBox
            // 
            this.scoreBox.FormattingEnabled = true;
            this.scoreBox.Location = new System.Drawing.Point(9, 60);
            this.scoreBox.Name = "scoreBox";
            this.scoreBox.Size = new System.Drawing.Size(179, 95);
            this.scoreBox.TabIndex = 11;
            // 
            // backToHandButton
            // 
            this.backToHandButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.backToHandButton.Location = new System.Drawing.Point(35, 1);
            this.backToHandButton.Name = "backToHandButton";
            this.backToHandButton.Size = new System.Drawing.Size(86, 23);
            this.backToHandButton.TabIndex = 14;
            this.backToHandButton.Text = "back to hand";
            this.backToHandButton.UseVisualStyleBackColor = false;
            this.backToHandButton.Click += new System.EventHandler(this.backToHandButton_Click);
            // 
            // ScrabblePlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(470, 345);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ScrabblePlayerForm";
            this.Text = "ScrabblePlayerForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScrabblePlayerForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Panel boardPanel;
        private Panel handPanel;
        private TableLayoutPanel tableLayoutPanel1;
        private Label pickedLabel;
        private Panel panel1;
        private Label TurnLabel;
        private Button connectButton;
        private TextBox portBox;
        private TextBox IpBox;
        private TextBox passwordBox;
        private Label StatusLabel;
        private Label IdLabel;
        private Panel panel2;
        private ListBox scoreBox;
        private TextBox nickBox;
        private Label label1;
        private Button endTurnButton;
        private Button backToHandButton;
    }
}