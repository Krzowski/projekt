﻿using System.Windows.Forms;

namespace WinClient
{
    public static class InfoControl
    {
        public static Label IdLabel { get; set; }
        public static Label StatusLabel { get; set; }
        public static Label TurnLabel { get; set; }
        public static void SetId(int i) => IdLabel.Text = "My id: " + i;
        public static void SetId(string s) => IdLabel.Text = "My id: " + s;
        public static void SetStatus(string status) => StatusLabel.Text = "My status: " + status;

        public static void SetTurn(bool x)
        {
            if (TurnLabel.InvokeRequired)
            {
                SetTurnDelegate d = SetTurn;
                TurnLabel.Invoke(d, x);
                return;
            }
            TurnLabel.Text = x ? "MY TURN" : "not my turn";

        }
        

        delegate void SetTurnDelegate(bool x);
    }
}