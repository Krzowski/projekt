﻿using System.Collections.Generic;
using CommonClasses;

namespace WinClient
{
    static class ClientsideLogic
    {
        public static List<ScrabbleLetter> LastValidHand = new List<ScrabbleLetter>();
        public static List<ScrabbleLetter> LastValidBoard = new List<ScrabbleLetter>();
    }
}
