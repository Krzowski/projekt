﻿namespace WinClient
{
    public class Multiplier
    {
        public int ScoreMultiplier { get; set; }
        public bool IsWordMultiplier { get; set; }

        public Multiplier(int s, bool w)
        {
            ScoreMultiplier = s;
            IsWordMultiplier = w;
        }
    }
}