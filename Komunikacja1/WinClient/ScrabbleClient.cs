﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using CommonClasses;
using Lidgren.Network;

namespace WinClient
{
    public static class ScrabbleClient
    {
        public static bool Connected { get; set; }
        public static bool Active { get; set; }
        static NetClient _client;
        private static Thread _wgsThread;
        public static bool Xcancel { get; set; }
        public static ScrabblePlayerForm PlayerForm;

        public static void Setup(ScrabblePlayerForm s)
        {
            PlayerForm = s;
            var config = new NetPeerConfiguration("kom1");
            InfoControl.SetStatus("Not connected");
            InfoControl.SetId("???");
            config.SetMessageTypeEnabled(NetIncomingMessageType.UnconnectedData, true);
            _client = new NetClient(config);
            _client.Start();
        }

        private static void ReceiveLetters()
        {
            NetIncomingMessage message;
            while (true)
            {
                if ((message = _client.ReadMessage()) == null) continue;
                if (message.MessageType == NetIncomingMessageType.Data)
                {
                    var x = new XmlSerializer(typeof(List<ScrabbleLetter>));
                    var receivedString = message.ReadString();
                    object result;
                    
                    using (TextReader reader = new StringReader(receivedString))
                    {
                        result = x.Deserialize(reader);
                    }

                    if (result != null) Hand.Letters.AddRange(result as List<ScrabbleLetter>);
                    break;
                }
            }
            PlayerForm.SaveToLastValid();
        }

        private static void Timeout()
        {
            for (int i = 0; i < 10; i++)
            {
                if (_client.ConnectionsCount > 0) return;
                Thread.Sleep(500);
            }
        }

        public static void Connect(string ip, string port, string password)
        {
            if (Connected) return;
            _wgsThread?.Join();
            var approval = _client.CreateMessage();
            approval.Write(password);
            _client.Connect(ip, 14242, approval);
            var timeoutThread = new Thread(Timeout);
            timeoutThread.Start();
            timeoutThread.Join();
            if (_client.ConnectionsCount == 0)
            {
                InfoControl.SetStatus("Server not found");
                return;
            }
            NetIncomingMessage response;
            while (!Connected)
            {
                Thread.Sleep(1);
                if ((response = _client.ReadMessage()) == null) continue;
                if (response.MessageType == NetIncomingMessageType.UnconnectedData)
                {
                    var nick = response.ReadString();
                    CheckNick(nick);
                }
            }
            _wgsThread=new Thread(WaitGameStart);
            Fred.AddStart(_wgsThread);
        }

        private static void CheckNick(string nick)
        {
            if (int.Parse(nick) > 0)
            {
                Connected = true;
                InfoControl.SetStatus("Connected");
                InfoControl.SetId(nick);
            }
            else
            {
                InfoControl.SetStatus("Connection not accepted");
            }
        }

        public static void WaitGameStart()
        {
            NetIncomingMessage response;
            bool game = false;
            Xcancel = false;
            while (!game && !Fred.ByeThreads && !Xcancel)
            {
                Thread.Sleep(1);
                if ((response = _client.ReadMessage()) == null) continue;
                if (response.MessageType == NetIncomingMessageType.Data)
                {
                    var s = response.ReadString();
                    var x = s.Equals(Commands.GameStarted);
                    if (x) Fred.AddStart(new Thread(Play));
                    else GameCanceled();
                    game = true;
                }
            }
        }
           
        public static void Play()
        {
            NetIncomingMessage srvMessage;
            InfoControl.SetTurn(false);
            _wgsThread?.Join();
            Xcancel = false;
            var rlThread = new Thread(ReceiveLetters);
            rlThread.Start();
            rlThread.Join();
            PlayerForm.PrepareForGame();
            while (!Fred.ByeThreads && !Xcancel)
            {
                Thread.Sleep(1);
                if ((srvMessage = _client.ReadMessage()) == null) continue;
                if (srvMessage.MessageType == NetIncomingMessageType.Data)
                {
                    var x = srvMessage.ReadString();
                    if (x.Equals(Commands.Wait))
                    {
                        Active = false;
                        InfoControl.SetTurn(false);
                        if (PlayerForm.EndTurn())
                        {
                            SendBoardToServer();
                            ReceiveLetters();
                        }

                        string board = ReceiveBoard();
                        if (!board.Equals("")) PlayerForm.DeserializeToLastValid(board);
                        PlayerForm.LoadFromLastValid();
                        PlayerForm.DeactivateAllTiles();
                    }
                    else if (x.Equals(Commands.Turn))
                    {
                        Active = true;
                        PlayerForm.IsItReallyMyTurn = true;
                        InfoControl.SetTurn(true);
                    }
                    else if (x.Equals(Commands.GameCanceled))
                    {
                        GameCanceled();
                    }
                    else if (x.Equals(Commands.GameOver))
                    {
                        GameOver();
                    }
                }
            }
        }

        private static string ReceiveBoard()
        {
            NetIncomingMessage message;
            while (true)
            {
                if ((message = _client.ReadMessage()) == null) continue;
                if (message.MessageType == NetIncomingMessageType.Data)
                {
                    return message.ReadString();
                }
            }
        }

        private static void SendBoardToServer()
        {
            MessageServer(PlayerForm.GetRequiredLetterCount() + PlayerForm.SerializeBoard());
        }
        private static void MessageServer(string s)
        {
            var msg = _client.CreateMessage();
            msg.Write(s);
            _client.SendMessage(msg, _client.ServerConnection, NetDeliveryMethod.ReliableOrdered);
        }
        private static void GameCanceled()
        {
            Xcancel = true;
            Connected = false;
            _client.Disconnect("");
            InfoControl.SetStatus("Disconnected - game canceled");
            InfoControl.SetId("---");
            //InfoControl.SetTurn(false);
        }

        private static void GameOver()
        {
            Xcancel = true;
            Active = false;
            MessageBox.Show("Game over!");
        }

        public static void Disconnect()
        {
            _client.Disconnect("disconnecting");
        }

        public static void RequestEndTurnEarly()
        {
            MessageServer(Commands.EndTurn);
        }
    }
}