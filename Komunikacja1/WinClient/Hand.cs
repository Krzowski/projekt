﻿using System.Collections.Generic;
using System.Deployment.Application;
using System.Windows.Forms;
using CommonClasses;

namespace WinClient
{
    public static class Hand
    {
        public static ScrabbleLetter Picked { get; set; }
        public static Label PickedLabel { get; set; }
        public static List<ScrabbleLetter> Letters = new List<ScrabbleLetter>();
        public static Panel Panel { get; set; }
        public static bool Selected()
        {
            return Picked != null;
        }
        public static void Place(ScrabbleTile target)
        {
            target.Letter = Picked;
            Picked = null;
            target.UpdateStyle();
            UpdateLabel();
        }

        public static void Drop()
        {
            Picked = null;
            UpdateLabel();
        }
        public static void Take(ScrabbleTile source)
        {
            if (!source.Active) return;
            Picked = source.Letter;
            source.Letter = null;
            source.UpdateStyle();
            UpdateLabel();
            if (!(source is ScrabbleTileOnBoard))
            {
                Letters.Remove(source.Letter);
            }
        }

        private static void UpdateLabel()
        {
            if (Picked == null)
            {
                PickedLabel.Text = "";
                PickedLabel.Visible = false;
            }
            else
            {
                PickedLabel.Text = Picked.ToString();
                PickedLabel.Visible = true;
            }
        }

        public static void BackToHand()
        {
            if (Picked == null) return;
            Letters.Add(Picked);
            Drop();
        }
    }
}
