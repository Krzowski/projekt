﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CommonClasses;
using Newtonsoft.Json;

namespace WinClient
{
    public partial class ScrabblePlayerForm : Form
    {
        private int _myPoints;
        private int _xDim=15;
        private int _yDim = 15;
        private static ScrabbleTile[,] _buttonMatrix;
        private static bool _isStartingTurn = true;
        private static List<ScrabbleLetter> _lastValidHand;
        private static List<ScrabbleLetter> _lastValidBoard;
        private static BindingList<ScoreDto> scoreboard = new BindingList<ScoreDto>(); 
        public bool IsItReallyMyTurn { get; set; }

        public ScrabblePlayerForm()
        {
            InitializeComponent();
            Fred.ByeThreads = false;
            InfoControl.TurnLabel = TurnLabel;
            InfoControl.IdLabel = IdLabel;
            GenerateBoard();
            InfoControl.StatusLabel = StatusLabel;
            ScrabbleClient.Setup(this);
            Hand.PickedLabel = pickedLabel;
            scoreBox.DataSource = scoreboard;
        }

        private void GenerateBoard()
        {
            _buttonMatrix = new ScrabbleTileOnBoard[_yDim, _xDim];
            for (int x = 0; x < _yDim; x++)
            {
                for (int y = 0; y < _xDim; y++)
                {
                    var sT = new ScrabbleTileOnBoard
                    {
                        Parent = boardPanel,
                        Xpos = x,
                        Ypos = y,
                        Mul = new Multiplier(MultiplierFromXy(x, y), IsWordMulFromXy(x, y)),
                        Letter = null,
                        Location = new Point(x * AppStyle.TileSize, y * (AppStyle.TileSize - 2))
                    };
                    sT.Click += MatrixButtonClick;
                    sT.UpdateStyle();
                    _buttonMatrix[x, y] = sT;
                    boardPanel.Controls.Add(sT);
                }
            }
        }

        private bool CheckBoard()
        {
            var list = GetListOfWords();
            if (list.Count == 0) return false;
            if (!StartsFromCenterOnFirstTurn(list)) return false;
            return CheckListOfWords(list);
        }

        private void LoadBoard(List<ScrabbleLetter> list)
        {
            if (list.Count == 0) return;

            for (int x = 0; x < _yDim; x++)
            {
                for (int y = 0; y < _xDim; y++)
                {
                    _buttonMatrix[y, x].Letter = list[y * _xDim + x];
                    _buttonMatrix[y, x].UpdateStyle();
                }
            }
        }
        public void LoadFromLastValid()
        {
            if (InvokeRequired)
            {
                LoadFromLastValidDelegate d = LoadFromLastValid;
                Invoke(d);
                return;
            }
            LoadHand(_lastValidHand);
            LoadBoard(_lastValidBoard);
        }

        public void SaveToLastValid()
        {
            if (InvokeRequired)
            {
                SaveToLastValidDelegate d = SaveToLastValid;
                Invoke(d);
            }
            else
            {
                _lastValidHand = SaveHand();
                _lastValidBoard = SaveBoard();
            }
        }

        public string SerializeBoard()
        {
            if (InvokeRequired)
            {
                SerializeBoardDelegate d = SerializeBoard;
                return Invoke(d) as string;
            }
            GameStateDto g = new GameStateDto() {board = _lastValidBoard, myScore = new ScoreDto() {Name = nickBox.Text, Score=_myPoints} };
            string s = JsonConvert.SerializeObject(g);
            return s;
        }

        public List<ScrabbleLetter> DeserializeBoard(string s)
        {
            GameStateDto g = JsonConvert.DeserializeObject<GameStateDto>(s);
            CopyScoreboard(g.Scores);
            return g.board;
        }

        private void CopyScoreboard(List<ScoreDto> scores)
        {
            scoreboard.Clear();
            foreach (var x in scores)
            {
                scoreboard.Add(new ScoreDto(x));
            }
        }

        private void UpdateHand()
        {
            handPanel.Controls.Clear();
            int x = 0;
            int y = 0;
            foreach (var l in Hand.Letters)
            {
                var sT = new ScrabbleTileInHand
                {
                    Parent = handPanel,
                    Letter = l,
                    Location = new Point(x * AppStyle.TileSize, y * (AppStyle.TileSize - 2))
                };
                sT.Click += MatrixButtonClick;
                sT.UpdateStyle();
                handPanel.Controls.Add(sT);
                x++;
                if (x < 15) continue;
                x = 0;
                y++;
            }
        }

        private List<ScrabbleWord> GetListOfWords()
        {
            List<ScrabbleWord> list = new List<ScrabbleWord>();
            ScrabbleWord w = new ScrabbleWord();

            for (int x = 0; x < _xDim; x++)
            {
                for (int y = 0; y < _yDim; y++)
                {
                    if (_buttonMatrix[y, x].Letter == null)
                    {
                        if (w.Tiles.Count == 0) continue;
                        if (w.Tiles.Count >= 1 && ContainsActiveAndInactiveUnlessStarting(w)) list.Add(w);
                        w = new ScrabbleWord();
                    }
                    else
                    {
                        w.Tiles.Add(_buttonMatrix[y, x]);
                    }
                    if (y == _yDim - 1)
                    {
                        if(ContainsActiveAndInactiveUnlessStarting(w)) list.Add(w);
                        w = new ScrabbleWord();
                    }
                }
            }
            for (int x = 0; x < _xDim; x++)
            {
                for (int y = 0; y < _yDim; y++)
                {
                    if (_buttonMatrix[x, y].Letter == null)
                    {
                        if (w.Tiles.Count == 0) continue;
                        if (w.Tiles.Count >= 1 && ContainsActiveAndInactiveUnlessStarting(w)) list.Add(w);
                        w = new ScrabbleWord();
                    }
                    else
                    {
                        w.Tiles.Add(_buttonMatrix[x, y]);
                    }
                    if (y == _yDim - 1)
                    {
                        if (ContainsActiveAndInactiveUnlessStarting(w)) list.Add(w);
                        w = new ScrabbleWord();
                    }
                }
            }
            return list;
        }

        private bool ContainsActiveAndInactiveUnlessStarting(ScrabbleWord scrabbleWord)
        {
            if (_isStartingTurn) return true;
            bool activeFound = false;
            bool inactiveFound = false;
            foreach (var t in scrabbleWord.Tiles)
            {
                if (t.Active) activeFound = true;
                else inactiveFound = true;
            }
            return activeFound && inactiveFound;
        }

        private int CheckPoints()
        {
            var list = GetListOfWords();
            return CountPointsInList(list);
        }

        private int CountPointsInList(List<ScrabbleWord> list)
        {
            int total=0;
            foreach (var w in list)
            {
                if (!w.IsValid()) continue;
                int wordPoints = 0;
                foreach (var t in w.Tiles)
                {
                    int mul = t.Mul.IsWordMultiplier ? 1 : t.Mul.ScoreMultiplier;
                    wordPoints += t.Letter.Score*mul;
                }
                foreach (var t in w.Tiles)
                {
                    if (t.Mul.IsWordMultiplier)
                        wordPoints *= t.Mul.ScoreMultiplier;
                }
                total += wordPoints;
            }
            return total;
        }

        private bool CheckListOfWords(List<ScrabbleWord> list)
        {
            foreach (var w in list)
            {
                if (!w.IsValid()) return false;
            }
            return true;
        }

        private void LoadHand(List<ScrabbleLetter> list )
        {
            ClearHand();
            if (list.Count == 0) return;

            for (int i = 0; i < list.Count; i++)
            {
                Hand.Letters.Add(list[i]);
            }
            UpdateHand();
        }

        private List<ScrabbleLetter> SaveBoard()
        {
            List<ScrabbleLetter> list = new List<ScrabbleLetter>();
            foreach (var t in _buttonMatrix)
            {
                list.Add(t.Letter);
            }
            return list;
        }

        private List<ScrabbleLetter> SaveHand()
        {
            List<ScrabbleLetter> list = new List<ScrabbleLetter>();
            foreach (var t in Hand.Letters)
            {
                list.Add(t);
            }
            if (Hand.Picked != null) list.Add(Hand.Picked);
            return list;
        }

        private void ClearHand()
        {
            Hand.Drop();
            Hand.Letters.Clear();
        }

        private void MatrixButtonClick(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                MatrixButtonClickDelegate d = MatrixButtonClick;
                Invoke(d, sender, e);
            }
            else
            {
                if (!ScrabbleClient.Active)
                {
                    MessageBox.Show("It's not your turn!");
                    return;
                }
                bool inhand = sender is ScrabbleTileInHand;
                if (!(sender is ScrabbleTile)) return;
                ScrabbleTile b = sender as ScrabbleTile;
                if (Hand.Selected() && b.Empty())
                {
                    Hand.Place(b);
                    if (CheckBoard()) SaveToLastValid();
                }
                else if (!Hand.Selected() && !b.Empty())
                {
                    if (inhand) Hand.Letters.Remove(b.Letter);
                    Hand.Take(b);
                    if (inhand) UpdateHand();
                }
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            ScrabbleClient.Connect(IpBox.Text, portBox.Text, passwordBox.Text);
        }

        public void DeactivateAllTiles()
        {
            if (InvokeRequired)
            {
                DeactivateAllTilesDelegate d = DeactivateAllTiles;
                Invoke(d);
                return;
            }
            bool anything = false;
            foreach (var t in _buttonMatrix)
            {
                if (!t.Empty())
                {
                    anything = true;
                    t.Active = false;
                    t.UpdateStyle();
                }
            }
            if(anything) _isStartingTurn = false;
        }

      

        public bool EndTurn()
        {
            if (!IsItReallyMyTurn) return false;
            if (InvokeRequired)
            {
                EndTurnDelegate d = EndTurn;
                return (bool)Invoke(d);
            }
            LoadFromLastValid();
            _myPoints += CheckPoints();
            DeactivateAllTiles();
            IsItReallyMyTurn = false;
            return true;
        }

        public void PrepareForGame()
        {
            if (InvokeRequired)
            {
                PrepareForGameDelegate d = PrepareForGame;
                Invoke(d);
            }
            else
            {
                UpdateHand();
                SaveToLastValid();
            }
        }    

        private void ScrabblePlayerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ScrabbleClient.Disconnect();
            ScrabbleClient.PlayerForm = null;
            Fred.JoinAll();
        }

        public void DeserializeToLastValid(string board)
        {
            _lastValidBoard = DeserializeBoard(board);
        }

        public int GetRequiredLetterCount()
        {
            return 7 - Hand.Letters.Count;
        }

        private bool StartsFromCenterOnFirstTurn(List<ScrabbleWord> list)
        {
            if (!_isStartingTurn) return true;
            foreach (var w in list)
            {
                foreach (var t in w.Tiles)
                {
                    var tb = t as ScrabbleTileOnBoard;
                    if (tb.Xpos == 7 && tb.Ypos == 7) return true;
                }
            }
            return false;
        }

        private bool IsWordMulFromXy(int x, int y)
        {
            switch (AppStyle.BoardString[x + y * _xDim])
            {
                case '0':
                case '2':
                case '4':
                case '5':
                    return false;
            }
            return true;
        }

        private int MultiplierFromXy(int x, int y)
        {
            switch (AppStyle.BoardString[x + y * _xDim])
            {
                case '0':
                    return 1;
                case '1':
                    return 3;
                case '2':
                    return 2;
                case '3':
                    return 2;
                case '4':
                    return 3;
                case '5':
                    return 1;
                default:
                    return 0;
            }
        }

        delegate void MatrixButtonClickDelegate(object sender, EventArgs e);

        delegate void LoadFromLastValidDelegate();

        delegate void SaveToLastValidDelegate();

        delegate void DeactivateAllTilesDelegate();

        delegate bool EndTurnDelegate();

        delegate void PrepareForGameDelegate();

        delegate string SerializeBoardDelegate();

        private void endTurnButton_Click(object sender, EventArgs e)
        {
            if (!IsItReallyMyTurn) return;
            ScrabbleClient.RequestEndTurnEarly();
        }

        private void backToHandButton_Click(object sender, EventArgs e)
        {
            Hand.BackToHand();
            if(CheckBoard()) SaveToLastValid();
            UpdateHand();
        }
    }
}
